using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NeighboursCompilation : MonoBehaviour
{
    public Transform model;
    public GameObject panel;
    private bool currentState = false;
    private int numberOfMissions;
    public TextMeshProUGUI missionText;
    public Slider slider;
    private List<Plane> indexPlanes= new List<Plane>();
    private Dictionary<int, List<Plane>> indexMap = new Dictionary<int, List<Plane>>();
    private List<Transform> selectedNeighbours = new List<Transform>();
    public GameObject indexButton;
    private int currentIndex = 0;
    private Transform currentPlane;
    // Start is called before the first frame update
    void Start()
    {   
        indexPlanes = Container.GetPlanes();
        indexMap = Container.GetIndexMap();
    }

    // Update is called once per frame
    void Update()
    {
        # if UNITY_EDITOR
        if(Input.GetMouseButtonDown(0)){
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)){
                if(hit.collider!=null)
                {
                    Collider newElement = hit.collider;
                    Color color = newElement.GetComponent<MeshRenderer>().material.color;
                    if(newElement.GetComponent<MeshRenderer>().material.color.Equals(Color.red)){
                        Debug.Log("Cant shoose already selected plane " + newElement.name);
                    }
                    else{
                        Plane temp = indexPlanes.Find(t => t.name.Equals(newElement.name));
                        List<Plane> tempPlanes = null;
                        if (temp != null && indexMap.ContainsKey(temp.id))
                        {
                            tempPlanes = indexMap[temp.id];
                            foreach (Plane plane in tempPlanes)
                            {

                                if (plane.id == temp.id)
                                {
                                    Debug.Log("Plane " + plane);
                                    model.Find(plane.name).GetComponent<MeshRenderer>().material.color = Color.blue;
                                }
                                else break;
                            }
                        }
                        else{
                            newElement.GetComponent<MeshRenderer>().material.color = Color.blue;
                        }
                    }
                    Debug.Log("Hit element :" + newElement.name);

                }
            }
        }
#endif
#if UNITY_ANDROID
if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began){
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)){
                if(hit.collider!=null)
                {
                    Collider newElement = hit.collider;
                    Color color = newElement.GetComponent<MeshRenderer>().material.color;
                    if(newElement.GetComponent<MeshRenderer>().material.color.Equals(Color.red)){
                        Debug.Log("Cant shoose already selected plane " + newElement.name);
                    }
                    else{
                        Plane temp = indexPlanes.Find(t => t.name.Equals(newElement.name));
                        List<Plane> tempPlanes = null;
                        if (temp != null && indexMap.ContainsKey(temp.id))
                        {
                            tempPlanes = indexMap[temp.id];
                            foreach (Plane plane in tempPlanes)
                            {

                                if (plane.id == temp.id)
                                {
                                    Debug.Log("Plane " + plane);
                                    model.Find(plane.name).GetComponent<MeshRenderer>().material.color = Color.blue;
                                }
                                else break;
                            }
                        }
                        else{
                            newElement.GetComponent<MeshRenderer>().material.color = Color.blue;
                        }
                    }
                    Debug.Log("Hit element :" + newElement.name);

                }
            }
        }
    #endif
    }
    
    public void ChangeNeighbors(){
        indexMap = Container.GetIndexMap();
        indexPlanes = Container.GetPlanes();
        foreach(Transform item in model){
            item.GetComponent<MeshRenderer>().material.color = Color.grey;
        }
        panel.SetActive(!currentState);
        GetComponent<ChangingColor>().enabled = currentState;
        GetComponent<NeighboursCompilation>().enabled = !currentState;
        indexButton.SetActive(currentState);
        currentState = !currentState;
        Debug.Log("Ended Index Compilation" + currentState);
        if(currentState){
            currentPlane = model.GetChild(currentIndex);
            currentPlane.GetComponent<MeshRenderer>().material.color = Color.red;
            Plane temp = indexPlanes.Find(t => t.name.Equals(currentPlane.name));
            SetIndexes(temp);
        }
    }
    public void NextIndex(){
        Debug.Log("Next index method with "+ currentIndex);
        if(currentIndex < model.childCount)
        {
            //TODO Save neighbours to container
            List<Transform> saveAllIndex = FindAllIndexPlanes();
            if(saveAllIndex != null){
                foreach(Transform item in saveAllIndex){
                    SaveNeighboursById(item.GetSiblingIndex());
                    Debug.Log("Sibling " + item.GetSiblingIndex());
                }
            }
            else{
                SaveNeighbours();
            }
            foreach (Transform item in model)
            {
                item.GetComponent<MeshRenderer>().material.color = Color.grey;
            }

            currentIndex++;
            bool checkNextIndex = false;
            for(int i = 1; i < model.childCount+1; i++){
                if(Container.neighbours[currentIndex+1, i] != -1){

                    checkNextIndex = true;
                    break;
                   
                }
            }
            if(checkNextIndex) {
                NextIndex();
            }
            if(currentIndex < model.childCount){
                currentPlane = model.GetChild(currentIndex);
                currentPlane.GetComponent<MeshRenderer>().material.color = Color.red;
                Plane temp = indexPlanes.Find(t => t.name.Equals(currentPlane.name));
                SetIndexes(temp);
            }
            else{
                Debug.Log("It was last save");
                GetComponent<ChangingColor>().SaveDefault();
            }

        }
        else
        {
            Debug.Log("Neighbours not selected or index out of bounds ");
        }


    }
    private List<Transform> FindAllIndexPlanes(){
        List<Transform> result = new List<Transform>();
        List<Plane> tempPlanes = null;
        Plane temp = indexPlanes.Find(t => t.name.Equals(currentPlane.name));
        if (temp != null && indexMap.ContainsKey(temp.id))
        {
            tempPlanes = indexMap[temp.id];
            foreach (Plane plane in tempPlanes)
            {

                if (plane.id == temp.id)
                {

                    Debug.Log("Plane " + plane);
                    result.Add(model.Find(plane.name));
                }
                else break;
            }
        }
        else{
            return null;
        }
        return result;
    }

    private void SetIndexes(Plane temp)
    {
        List<Plane> tempPlanes = null;
        
        if (temp != null && indexMap.ContainsKey(temp.id))
        {
            tempPlanes = indexMap[temp.id];
            foreach (Plane plane in tempPlanes)
            {

                if (plane.id == temp.id)
                {
                    Debug.Log("Plane " + plane);
                    model.Find(plane.name).GetComponent<MeshRenderer>().material.color = Color.red;
                }
                else break;
            }
        }
    }
    

    public void NumberOfMissions(){
        
        numberOfMissions =(int)slider.value;
        missionText.text = "Number of Missions: " + numberOfMissions;
    }
    public void SaveNeighbours(){
        int index = 0;
        Container.neighbours[currentIndex+1, currentIndex+1] = (int)slider.value;
        foreach(Transform item in model){
            if(item.GetComponent<MeshRenderer>().material.color  == Color.blue){
                
                Container.neighbours[currentIndex+1, index+1] = 1;
                Debug.Log("Red index "+model.GetChild(currentIndex).name+" "+ (currentIndex+1) + " Neighbour index " + (index+1) + " item "+item.name+ " Slider missions "+ (int)slider.value);
                Debug.Log("Check correct element" + item.Equals(model.GetChild(index)));

            }
            index++;
        }
    }
    public void SaveNeighboursById(int id){
        int index = 0;
        Container.neighbours[id+1, id+1] = (int)slider.value;
        foreach(Transform item in model){
            if(item.GetComponent<MeshRenderer>().material.color  == Color.blue){
                Container.neighbours[id+1, index+1] = 1;
                Debug.Log("Red index "+model.GetChild(id).name+" "+ (id+1) + " Neighbour index " + (index+1) + " item "+item.name + " Slider missions "+ (int)slider.value);
                Debug.Log("Check correct element" + item.Equals(model.GetChild(index)));

            }
            index++;
        }
    }
    
}
