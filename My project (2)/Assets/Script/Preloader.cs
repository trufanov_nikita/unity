using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Preloader : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    private float loadTime;
    private float minimumLogoTime = 3.0f;

    private void Start()
    {
        
        canvasGroup = FindObjectOfType<CanvasGroup>();

        canvasGroup.alpha = 1;

        //Pre load the game
        //TODOO load saves from file
        if (Time.time < minimumLogoTime)
            loadTime = minimumLogoTime;
        else loadTime = Time.time;
        

    }

    private void Update()
    {
        //Fade in
        if(Time.time < minimumLogoTime)
        {
            
            canvasGroup.alpha = 1 - Time.time;
        }

        //Fade out
        if(Time.time > minimumLogoTime && loadTime != 0)
        {
            canvasGroup.alpha = Time.time - minimumLogoTime;
            if(canvasGroup.alpha >= 1)
            {
                SceneManager.LoadScene("Menu");
                Debug.Log("Change the scene");
            }
        }
    }
}
