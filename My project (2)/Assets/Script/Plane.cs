using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Plane {
    public int id;
    public string name;
    public int numberOfLevels;
    public int numberOfCompletedLevels;
    [Newtonsoft.Json.JsonIgnore]
    public Color color = Color.red;
    [Newtonsoft.Json.JsonIgnore]
    public Transform transform;
    public Boolean completed = false;
    public Plane(){
        
    }
    public Plane(int id, string name, int numberOfLevels, int numberOfCompletedLevels){
        this.id = id;
        this.name = name;
        this.numberOfLevels = numberOfLevels;
        this.numberOfCompletedLevels = numberOfCompletedLevels;
        
    }
        public Plane(int id, string name, int numberOfLevels, int numberOfCompletedLevels, Transform transform){
        this.id = id;
        this.name = name;
        this.numberOfLevels = numberOfLevels;
        this.numberOfCompletedLevels = numberOfCompletedLevels;
        this.transform = transform;
    }
    public Plane(int id, string name, Color color){
        this.id = id;
        this.name = name;
        this.color = color;
    }
    override
    public string ToString(){
        return "Plane [id: " +id+ ",name: "+name+", levels: "+numberOfLevels.ToString()+", Color: "+color.ToString()+"] ";
    }

    public override bool Equals(object obj)
    {
        return obj is Plane plane &&
               id == plane.id &&
               name == plane.name &&
               numberOfLevels == plane.numberOfLevels;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(id, name, numberOfLevels, numberOfCompletedLevels, color);
    }
}


