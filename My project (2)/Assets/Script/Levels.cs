using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Levels : MonoBehaviour
{
    private CanvasGroup[] fadeGroup;
    private Canvas[] canvases;
    private float fadeSpeed = 0.33f;
    private int currentMenuIndex = 0;
    private int currentGolemIndex;
    private int currentUnicornIndex;
    // Start is called before the first frame update
    public Transform levelPannel;
    public Transform unicornPannel;
    public RectTransform menuContainer;
    private Vector2 menuPosition;
    void Start()
    {
        currentGolemIndex = -1;
        currentUnicornIndex = -1;
        fadeGroup = FindObjectsOfType<CanvasGroup>();
        foreach(var item in fadeGroup){
            item.alpha = 1;
            Debug.Log("Fade" + item.name);
        }
       
        InitLevelSelection();
        InitUnicornSelection();
    }

    // Update is called once per frame
    void Update()
    {
        foreach(var item in fadeGroup){
            item.alpha = 1 - Time.timeSinceLevelLoad * fadeSpeed;
        }
        menuContainer.anchoredPosition3D = Vector2.Lerp(menuContainer.anchoredPosition3D, menuPosition, 0.1f);

    }
    public void OnReturnClick(){
        SceneManager.LoadScene("Menu");
    }
    public void OnRihtButtonClick(){
        
        currentMenuIndex--;
        NavigateTo(currentMenuIndex);
    }
    public void OnLeftButtonClick(){
        currentMenuIndex++;
        NavigateTo(currentMenuIndex);
    }
    private void InitLevelSelection(){
        if(levelPannel == null){
            Debug.Log("panell is null");
        }
        int i = 0;
        foreach(Transform item in levelPannel){
            int currentIndex = i;
            Button b = item.GetComponent<Button>();
            b.onClick.AddListener(()=>OnLevelSelect(currentIndex));
            i++;
        }
    }
    private void InitUnicornSelection(){
        if(unicornPannel == null){
            Debug.Log("panell unicorn is null");
        }
        int i = 0;
        foreach(Transform item in unicornPannel){
            int currentIndex = i;
            Button b = item.GetComponent<Button>();
            b.onClick.AddListener(()=>OnUnicornSelect(currentIndex));
            i++;
        }
    }
    private void OnLevelSelect(int index){
        currentGolemIndex = index;
        Debug.Log("Number of level selected" + index);
    }
        private void OnUnicornSelect(int index){
        currentUnicornIndex = index;
        Debug.Log("Number of uni selected" + index);
    }

    public void OnGolemPlaySet()
    {
        if(currentGolemIndex != -1){
            Debug.Log("Selected level Golem: " + currentGolemIndex);
        }
        else{
            Debug.Log("Level not selected");
        }
    }
    public void OnUnicornPlaySet()
    {
        if(currentUnicornIndex != -1){
            Debug.Log("Selected level Unicorn : " + currentUnicornIndex);
        }
        else{
            Debug.Log("Level not selected");
        }
    }
    public void OnMoveToUnicorn(){
        SceneManager.LoadScene("Unicorn");
    }
    public void OnMoveToGolem(){
        SceneManager.LoadScene("Golem");
    }
    private void NavigateTo(int index){
        switch(index){
            default:
                
            case 0:
                currentMenuIndex = 0;
                menuPosition = Vector2.zero;
                break;
            case 1:
                menuPosition = Vector2.right * 800;
                break;
            case 2:
                menuPosition = Vector2.right * 800*index;
                break;
        }
        

    }
}
