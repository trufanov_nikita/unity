using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IndexCompilation : MonoBehaviour
{
    public Transform model;
    private int index;
    private int numberOfMissions;
    private bool currentState = false;
    public Dictionary<int, List<Plane>> indexMap;
    private List<Plane> planes = new List<Plane>();
    public TextMeshProUGUI indexText;
    public TextMeshProUGUI missionText;
    public GameObject panel;
    public GameObject neighboursButton;
    Color currentColor;
    
    // Start is called before the first frame update
    void Start()
    {
        Container.init();
       currentColor = Color.red;
       planes = Container.GetPlanes();
       index = 0;
       
    }

    // Update is called once per frame
    void Update()
    {
        
# if UNITY_EDITOR

        if(Input.GetMouseButtonDown(0)){
             Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            
            if(Physics.Raycast(ray, out hit)){
                if(hit.collider!=null){
                    Collider newElement = hit.collider;
                    Debug.Log(hit.collider.GetType());

                    if(newElement != null){
                        if(newElement.GetComponent<MeshRenderer>().material.color.Equals(Color.gray)){
                            newElement.GetComponent<MeshRenderer>().material.color = currentColor;
                            Plane plane = new Plane(index, newElement.name, currentColor);
                            planes.Add(plane);
                        }
                        else if(newElement.GetComponent<MeshRenderer>().material.color.Equals(currentColor)){
                            newElement.GetComponent<MeshRenderer>().material.color = Color.grey;
                            planes.Remove(new Plane(index, newElement.name, currentColor));
                        }
                        else{
                            Debug.Log("Already taken");
                        }
                        Debug.Log("Hit element :" + newElement.name);
                    }
                }
            }
        }
#endif
#if UNITY_ANDROID
 if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began){
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;
            
            if(Physics.Raycast(ray, out hit)){
                if(hit.collider!=null){
                    Collider newElement = hit.collider;
                    Debug.Log(hit.collider.GetType());

                    if(newElement != null){
                        if(newElement.GetComponent<MeshRenderer>().material.color.Equals(Color.gray)){
                            newElement.GetComponent<MeshRenderer>().material.color = currentColor;
                            Plane plane = new Plane(index, newElement.name, currentColor);
                            planes.Add(plane);
                        }
                        else if(newElement.GetComponent<MeshRenderer>().material.color.Equals(currentColor)){
                            newElement.GetComponent<MeshRenderer>().material.color = Color.grey;
                            planes.Remove(new Plane(index, newElement.name, currentColor));
                        }
                        else{
                            Debug.Log("Already taken");
                        }
                        Debug.Log("Hit element :" + newElement.name);
                    }
                }
            }
        }
    #endif
    }
    
    public void ChangeScript(){
        Debug.Log("Entered Index Compilation" + currentState);
        foreach(Transform item in model){
            item.GetComponent<MeshRenderer>().material.color = Color.grey;
        }
        foreach(Plane plane in Container.GetPlanes()){
            Debug.Log("Expected "+plane);
            model.Find(plane.name).GetComponent<MeshRenderer>().material.color = plane.color;
            Debug.Log("Model fount " + model.Find(plane.name).name);
        }
        panel.SetActive(!currentState);
        GetComponent<ChangingColor>().enabled = currentState; 
        if(GetComponent<ChangingColor>().enabled){
            GetComponent<ChangingColor>().UpdateData();
        }
        GetComponent<IndexCompilation>().enabled = !currentState;
        neighboursButton.SetActive(currentState);
        currentState = !currentState;
        Debug.Log("Ended Index Compilation" + currentState);
    }
    public void NextIndex(){
        Color newColor = new Color(Random.Range(0.0f,1.0f), Random.Range(0.0f,1.0f), Random.Range(0.0f,1.0f));
        currentColor = newColor;
        foreach(Plane item in planes){
            item.numberOfLevels = numberOfMissions;
        }
        Container.SavePlanes(planes);
        planes = new List<Plane>();
        index++;
        indexText.text = "Index: " + index;
    }
    public void NumberOfMissions(){
        
        numberOfMissions =(int)FindObjectOfType<Slider>().value;
        missionText.text = "Number of Missions: " + numberOfMissions;
    }
}

