using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class AfterFirstLevel : MonoBehaviour
{
    public string nameForContainer;
    public GameObject beforeSelectedLevelPanel;

    public GameObject afterSelectedLevelPanel;
    public GameObject button;
    public Transform model;
    public Transform backModel;
    public List<Transform> list = new List<Transform>();
    
    public List<Plane> listOfPlanes = new List<Plane>();

    private Plane currentLevel;
    private List<Plane> currentLevels;

    private int selectedIndex;

    private float currentFade = 0f;
    private float fadeDuration = 2f;

    // Start is called before the first frame update
    void Start()
    {
        //CreateCanvasAndLevelText();
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)){
                if(hit.collider!=null)
                {
                    Collider newElement = hit.collider;
                    foreach(var item in Container.indexMap){
                        print(item.Key + " " + item.Value);
                    }
                    int index = newElement.transform.GetSiblingIndex();
                    print("Level choosen " + name);
                    if (!listOfPlanes[index].completed && listOfPlanes[index].transform.GetComponent<MeshRenderer>().material.color == Color.blue)
                    {
                        if(currentLevels != null){
                            foreach(Plane plane in currentLevels){
                                if(plane.completed == false)
                                    plane.transform.GetComponent<MeshRenderer>().material.color = Color.blue;
                            }
                        }
                        if(currentLevel != null){
                            if(currentLevel.completed == false)
                                currentLevel.transform.GetComponent<MeshRenderer>().material.color = Color.blue;
                        }
                        listOfPlanes[index].transform.GetComponent<MeshRenderer>().material.color = Color.red;
                        Plane temp = Container.GetPlanes().Find(t => t.name.Equals(listOfPlanes[index].name));
                        if(temp != null && Container.indexMap.ContainsKey(temp.id)){
                            if(temp.id == 0){
                               currentLevels = Container.indexMap[temp.id].FindAll(t => t.id == 0); 
                            }
                            else{
                                currentLevels = Container.indexMap[temp.id];
                            }
                            for(int i = 0; i < currentLevels.Count; i++){
                                currentLevels[i] = listOfPlanes[model.Find(currentLevels[i].name).GetSiblingIndex()];
                                currentLevels[i].transform.GetComponent<MeshRenderer>().material.color = Color.red;
                            }
                            currentLevel = null;
                        }
                        else{
                            currentLevel = listOfPlanes[index];
                            currentLevels = null;
                        }
                    }
                }
            }
        }
    }
    public void CreateCanvasAndLevelText()
    {
        int counter = 0;
        foreach (Transform item in model)
        {

            GameObject canvas = new GameObject();
            GameObject text = new GameObject();


            canvas.name = "Canvas with text";
            canvas.AddComponent<Canvas>();
            canvas.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
            canvas.GetComponent<Canvas>().worldCamera = FindObjectOfType<Camera>();
            int missionNumber = Container.neighbours[counter + 1, counter + 1];

            

            text.name = "Text for " + item.name;
            TextMesh textMesh = text.AddComponent<TextMesh>();
            textMesh.text = "0/" + missionNumber;
            textMesh.fontSize = 80;
            Bounds bounds = item.GetComponent<MeshCollider>().bounds;
            Vector3 vector = bounds.center;
            text.transform.position = new Vector3(vector.x - 3, vector.y, -50);
            text.transform.SetParent(canvas.transform);
            canvas.transform.SetParent(item);
            canvas.SetActive(false);
            list.Add(canvas.transform.parent);
            listOfPlanes.Add(new Plane(counter, item.name, missionNumber, 0, canvas.transform.parent));
            counter++;
        }

    }


    public void HideBeforeSelected(Collider selectedPlane, List<Plane> selectedPlanes){
        beforeSelectedLevelPanel.SetActive(false);
        button.SetActive(true);
        afterSelectedLevelPanel.SetActive(true);
        GameObject.Find("Reset").SetActive(false);
        GetComponent<ChangingColor>().enabled = false;
        GetComponent<IndexCompilation>().enabled = false;
        GetComponent<NeighboursCompilation>().enabled = false;
        GetComponent<AfterFirstLevel>().enabled = true;
        //StartFadeIn();
        //FadeOut();
        Container.Load();
        if(!LoadProgress()){
            if(selectedPlane != null){
            currentLevel = listOfPlanes[selectedPlane.transform.GetSiblingIndex()];
            int index = model.Find(currentLevel.name).GetSiblingIndex();
            listOfPlanes[index].transform.GetComponent<MeshRenderer>().material.color = Color.red;
            AssignTextToPlane(index);
            currentLevel = listOfPlanes[index];
        }
            else if (selectedPlanes != null){
                selectedPlanes.RemoveAll(x => x.id != selectedPlanes[0].id);
                currentLevels = selectedPlanes;
                for(int i = 0; i < currentLevels.Count; i++){
                        int index = model.Find(currentLevels[i].name).GetSiblingIndex();
                        AssignTextToPlane(index);
                        currentLevels[i] = listOfPlanes[ model.Find(currentLevels[i].name).GetSiblingIndex()];
                        currentLevels[i].transform.GetComponent<MeshRenderer>().material.color = Color.blue;
                    
                }
            }
        }
        
    }
    public void StartFadeIn()
    {
        StartCoroutine(FadeIn());
    }

     public IEnumerator FadeIn()
    {
        Debug.Log("fade in start");
         model.gameObject.SetActive(false);
        backModel.gameObject.SetActive(true);
        foreach(Transform item in backModel){
            item.gameObject.SetActive(true);
        }
        // avoid concurrent routines
        StopAllCoroutines();

        yield return StartCoroutine(FadeTowards(60f));
        
         Debug.Log("fade in end");

    }

    public IEnumerator FadeOut()
    {
        // avoid concurrent routines
        StopAllCoroutines();

        yield return StartCoroutine(FadeTowards(0f));
         model.gameObject.SetActive(true);
         foreach(Transform item in backModel){
            item.gameObject.SetActive(false);
        }
    }

    private IEnumerator FadeTowards(float targetFade)
    {
       
        while (!Mathf.Approximately(currentFade, targetFade))
        {
            // increase or decrease the currentFade according to the fade speed
            if (currentFade < targetFade)
                currentFade += Time.deltaTime / fadeDuration;
            else
                currentFade -= Time.deltaTime / fadeDuration;

            // if you like you could even add some ease-in and ease-out here
            //var lerpFactor = Mathf.SmoothStep(0, 1, currentFade);
            Bloom bloom;
            backModel.GetComponent<Volume>().profile.TryGet<Bloom>(out bloom);
            bloom.intensity.value = currentFade;
           

        }
         yield return new WaitForSeconds(1.0f);
    }

    private void AssignTextToPlane(int index)
    {
        listOfPlanes[index].numberOfLevels = Container.neighbours[index + 1, index + 1];
        listOfPlanes[index].transform.GetChild(0).GetChild(0).GetComponent<TextMesh>().text = listOfPlanes[index].numberOfCompletedLevels + "/" + listOfPlanes[index].numberOfLevels;
        listOfPlanes[index].transform.GetChild(0).gameObject.SetActive(true);
    }

    public void PlaySelectedLevel(){
        if(currentLevel != null)
        {
            OperationsOnSelectedLevel(currentLevel.name);

        }
        else
        {
            foreach(Plane item in currentLevels){
                OperationsOnSelectedLevel(item.name);
            }
        }
        SaveProgress();
    }

    private void OperationsOnSelectedLevel(String name)
    {
        Debug.Log(model.Find(name));
        int index = model.Find(name).GetSiblingIndex();
        print("Level selected " + name);
        int missionNumber = Container.neighbours[index + 1, index + 1];
        if (!listOfPlanes[index].completed)
        {
            
            listOfPlanes[index].numberOfCompletedLevels++;
            
            if (listOfPlanes[index].numberOfCompletedLevels >= listOfPlanes[index].numberOfLevels)
            {
                listOfPlanes[index].completed = true;
                listOfPlanes[index].transform.gameObject.SetActive(false);
                backModel.GetChild(index).gameObject.SetActive(true);
                listOfPlanes[index].transform.GetChild(0).GetChild(0).GetComponent<TextMesh>().text = " ";
                OpenNeighboursLevels(index);
            }
            else{
                listOfPlanes[index].transform.GetChild(0).GetChild(0).GetComponent<TextMesh>().text = listOfPlanes[index].numberOfCompletedLevels + "/" + missionNumber;
            }
        }
        else
        {
            //TODO paint in true color
            listOfPlanes[index].transform.gameObject.SetActive(false);
            backModel.GetChild(listOfPlanes[index].transform.GetSiblingIndex()).gameObject.SetActive(true);
            OpenNeighboursLevels(index);
        }
    }

    public void OpenNeighboursLevels(int index){
        for(int i = 0; i < Container.neighbourSize-1; i++){      
            if(i != index && Container.neighbours[index+1, i+1].Equals(1)){
                print("index " + index + " i " + i + " newighbour " + Container.neighbours[i+1, i+1]);
                if(!listOfPlanes[i].completed && listOfPlanes[i].id == i){
                    listOfPlanes[i].transform.GetComponent<MeshRenderer>().material.color = Color.blue;
                    AssignTextToPlane(i);
                }
            }
        }
        
    }

    public void SaveProgress(){
        string saveText = JsonConvert.SerializeObject(listOfPlanes);
        File.WriteAllText(Application.persistentDataPath+"/saveProgress"+nameForContainer+".txt", saveText);
    }

    public bool LoadProgress()
    {
        if (File.Exists(Application.persistentDataPath + "/saveProgress"+nameForContainer+".txt"))
        {
            string saveText = File.ReadAllText(Application.persistentDataPath + "/saveProgress"+nameForContainer+".txt");

            List<Plane> saveObject = JsonConvert.DeserializeObject<List<Plane>>(saveText);
            
            if(saveObject != null && saveObject.Count > 0){
                listOfPlanes = saveObject;
                for(int i = 0; i < listOfPlanes.Count; i++){
                    listOfPlanes[i].transform = model.Find(listOfPlanes[i].name);
                }
                for(int i = 0; i < listOfPlanes.Count; i++){
                    if(listOfPlanes[i].completed){
                        listOfPlanes[i].transform = model.Find(listOfPlanes[i].name);
                        listOfPlanes[i].transform.gameObject.SetActive(false);
                        backModel.GetChild(listOfPlanes[i].transform.GetSiblingIndex()).gameObject.SetActive(true);
                        listOfPlanes[i].transform.GetChild(0).GetChild(0).GetComponent<TextMesh>().text = " ";
                        OpenNeighboursLevels(i);
                    }
                }
                return true;
            }
        }
        return false;
    }
    
}


            
