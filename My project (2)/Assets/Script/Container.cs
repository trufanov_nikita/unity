using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

public class Container
{
    public static int neighbourSize = 51;
    
    public static Dictionary<int, List<Plane>> indexMap = new Dictionary<int, List<Plane>>();
    
    public static List<Plane> planes = new List<Plane>();
    public static int[,] neighbours = new int[neighbourSize,neighbourSize]; 

    public static string saveFile;

    //next variables for saving

    public Dictionary<int, List<Plane>> map = new Dictionary<int, List<Plane>>();
    
    public List<Plane> list = new List<Plane>();
    public int[,] array = new int[neighbourSize,neighbourSize]; 

    public static void SavePlanes(List<Plane> items){
        planes.AddRange(items);
        indexMap.Add(items[0].id, items);

        
    }
    public static void SaveMap(Dictionary<int, List<Plane>> items){
        indexMap = items;
    }
    public static Dictionary<int, List<Plane>> GetIndexMap(){
        return indexMap;
    }
    public static List<Plane>GetPlanes(){
        return planes;
    }
    public static int[,] GetNeighbours(){
        return neighbours;
    }
    public static void SetIndexMap(Dictionary<int, List<Plane>> newMap){
        Container.indexMap = newMap;
    }
    public static void SetPlanes(List<Plane> newPlane){
        Container.planes = newPlane;
    }
    public static void SetNeighbours(int[,] newArray){
        Container.neighbours = newArray;
    }
    public static void init(){
        for (int row = 0; row < 50; row++)
        {
            for (int column = 0; column < 50; column++)
            {
                neighbours[row, column] = -1;
            }
        } 
    }
    public static void init(string name){
        switch (name){
            case "Unicorn": 
                saveFile = "/saveUnicorn.txt";
                neighbourSize = 51;
                neighbours = new int[neighbourSize,neighbourSize]; 
                 for (int row = 0; row < 50; row++)
                    {
                        for (int column = 0; column < 50; column++)
                        {
                            neighbours[row, column] = -1;
                        }
                    } 
            break;
            case "Golem": 
                saveFile = "/saveGolem.txt";
                neighbourSize = 83;
                neighbours = new int[neighbourSize,neighbourSize]; 
                 for (int row = 0; row < 82; row++)
                    {
                        for (int column = 0; column < 82; column++)
                        {
                            neighbours[row, column] = -1;
                        }
                    } 
            break;
        }
       
    }
    public Container(Dictionary<int, List<Plane>> map, List<Plane> list, int[,] array){
        this.map = map;
        this.list = list;
        this.array = array;
    }
    
    public static void Save(){ 
        Container container = new Container(Container.GetIndexMap(), Container.GetPlanes(), Container.GetNeighbours());
        string saveText = JsonConvert.SerializeObject(container);
        File.WriteAllText(Application.persistentDataPath+saveFile, saveText);
    }
    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + saveFile))
        {
            string saveText = File.ReadAllText(Application.persistentDataPath + saveFile);
            Container saveObject = JsonConvert.DeserializeObject<Container>(saveText);
            Container.SetIndexMap(saveObject.map);
            Container.SetPlanes(saveObject.list);
            Container.SetNeighbours(saveObject.array);
            
        }
    }

    public static void Clear(){
        Container.indexMap = new Dictionary<int, List<Plane>>();
        Container.planes = new List<Plane>();
        Container.neighbours = new int[neighbourSize,neighbourSize]; 
    }
}
