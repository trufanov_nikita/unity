using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangingColor : MonoBehaviour
{
    public Transform model;
    public string nameForContainer; //Unicorn for unicorn etc.
    private bool firstTime = true;
    private Collider currentElement;
    List<Plane> currentPlanes = new List<Plane>();
    List<Plane> previousPlanes = new List<Plane>();
    private Dictionary<int, List<Plane>> indexMap = new Dictionary<int, List<Plane>>();
    List<Plane> planes = new List<Plane>();
    // Start is called before the first frame update
    void Start()
    {
        Container.init(nameForContainer);
        LoadDefault();
        indexMap = Container.GetIndexMap();
        planes = Container.GetPlanes();
        if(firstTime){
            foreach(Transform item in model){
                Shader shader = item.GetComponent<MeshRenderer>().materials[0].shader;
                item.GetComponent<MeshRenderer>().material.color = Color.grey;

            }
        }
        GetComponent<AfterFirstLevel>().CreateCanvasAndLevelText();

        
    }

    // Update is called once per frame
    void Update()
    {     
# if UNITY_EDITOR
        if(Input.GetMouseButtonDown(0)){
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)){
                if(hit.collider!=null)
                {
                    Collider newElement = hit.collider;
                    Plane temp = planes.Find(t => t.name.Equals(newElement.name));
                    List<Plane> tempPlanes = null;
                    CheckPrevTiles();
                    tempPlanes = PaintCurrentTiles(temp, tempPlanes);
                    SetPrevElements( newElement, temp, tempPlanes);

                }
            }
        }
#endif
#if UNITY_ANDROID
    if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began){
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)){
                if(hit.collider!=null)
                {
                    Collider newElement = hit.collider;
                    Plane temp = planes.Find(t => t.name.Equals(newElement.name));
                    List<Plane> tempPlanes = null;
                    CheckPrevTiles();
                    tempPlanes = PaintCurrentTiles(temp, tempPlanes);
                    SetPrevElements( newElement, temp, tempPlanes);

                }
            }
        }
#endif
    
    }

    private void SetPrevElements(Collider newElement, Plane temp, List<Plane> tempPlanes)
    {
        Color newColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));

        if (tempPlanes != null)
        {
            currentPlanes = tempPlanes;
            previousPlanes = new List<Plane>();
            previousPlanes.AddRange(tempPlanes);
            currentElement = null;
        }
        else
        {
            newElement.GetComponent<MeshRenderer>().material.color = temp != null ? temp.color : newColor;
            currentElement = newElement;
            previousPlanes = null;
        }
    }

    private List<Plane> PaintCurrentTiles(Plane temp, List<Plane> tempPlanes)
    {
        if (temp != null && indexMap.ContainsKey(temp.id))
        {
            tempPlanes = indexMap[temp.id];
            foreach (Plane plane in tempPlanes)
            {
                if (plane.id == temp.id)
                {
                    model.Find(plane.name).GetComponent<MeshRenderer>().material.color = plane.color;
                }
                else break;
            }
        }

        return tempPlanes;
    }

    private void CheckPrevTiles()
    {
        if (currentElement != null)
        {
            currentElement.GetComponent<MeshRenderer>().material.color = Color.grey;
        }
        if (previousPlanes != null)
        {

            foreach (Plane plane in previousPlanes)
            {
                model.Find(plane.name).GetComponent<MeshRenderer>().material.color = Color.grey;

            }
        }
    }

    public void UpdateData(){
        indexMap = Container.GetIndexMap();
        planes = Container.GetPlanes();
        foreach(Transform item in model){
            item.GetComponent<MeshRenderer>().material.color = Color.grey;
        }
    }
    public void SelectFirstLevel(){
        foreach(Transform item in model){
            item.GetComponent<MeshRenderer>().material.color = Color.grey;
        }
        if(File.Exists(Application.persistentDataPath+"/save"+nameForContainer+".txt"))
            GetComponent<AfterFirstLevel>().HideBeforeSelected(currentElement, currentPlanes);
    }
    public void Reset(){
        Container.Clear();
        File.Delete(Application.persistentDataPath+"/saveProgress"+nameForContainer+".txt");
        File.Delete(Application.persistentDataPath+"/save"+nameForContainer+".txt");
        UpdateData();
    }
    public void SaveDefault(){
        Debug.Log("Path save " + Application.persistentDataPath+"/saveProgress"+nameForContainer+".txt");
        File.Delete(Application.persistentDataPath+"/saveProgress"+nameForContainer+".txt");
        File.Delete(Application.persistentDataPath+"/save"+nameForContainer+".txt");
        Container.Save();
    }
    public void LoadDefault(){
        Container.Load();
        UpdateData();
    }
    public void OnReturnClick(){
        Container.Clear();
        SceneManager.LoadScene("Levels");
    }
}
