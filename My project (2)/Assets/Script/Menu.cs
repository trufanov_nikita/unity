using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    private CanvasGroup fadeGroup;
    private float fadeSpeed = 0.33f;

    void Start()
    {
        fadeGroup = FindObjectOfType<CanvasGroup>();
        fadeGroup.alpha = 1;
    }

    // Update is called once per frame
    void Update()
    {
        fadeGroup.alpha = 1 - Time.timeSinceLevelLoad * fadeSpeed;

    }

    public void OnChooseLevelClick(){
        SceneManager.LoadScene("Levels");
    }
    public void OnExitClick(){
        
        Application.Quit ();
        Debug.Log("Game is exiting");
    }
}
